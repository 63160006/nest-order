import { IsNotEmpty, Length } from 'class-validator';
export class CreateCustomerDto {
  @IsNotEmpty()
  @Length(4, 16)
  name: string;

  @IsNotEmpty()
  age: number;

  @IsNotEmpty()
  @Length(4, 16)
  tel: string;

  @IsNotEmpty()
  gender: string;
}
