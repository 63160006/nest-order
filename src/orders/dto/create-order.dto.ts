import { IsNotEmpty } from 'class-validator';

class CreatedOrderItemDto {
  productId: number;
  amount: number;
}
export class CreateOrderDto {
  @IsNotEmpty()
  customerId: number;
  @IsNotEmpty()
  orderItems: CreatedOrderItemDto[];
}
